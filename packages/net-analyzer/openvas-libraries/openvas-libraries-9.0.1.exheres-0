# Copyright 2017 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

DOWNLOAD_ID="2420"

require cmake [ api=2 ]

SUMMARY="OpenVAS libraries"
HOMEPAGE="http://www.openvas.org"
DOWNLOADS="http://wald.intevation.org/frs/download.php/${DOWNLOAD_ID}/${PNV}.tar.gz"

LICENCES="GPL-2 LGPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ldap
    snmp
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-crypt/gpgme
        dev-libs/glib:2[>=2.32]
        dev-libs/gnutls[>=3.2.15]
        dev-libs/hiredis[>=0.10.1]
        dev-libs/libgcrypt
        dev-libs/libgpg-error
        dev-libs/libksba[>=1.0.7]
        dev-libs/libpcap
        net-libs/libssh[>=0.5.0]
        ldap? ( net-directory/openldap )
        snmp? ( net/net-snmp )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_STATIC:BOOL=FALSE
    -DBUILD_WITH_RADIUS:BOOL=FALSE
    -DDATADIR:PATH=/usr/share
    -DLIBDIR:PATH=/usr/$(exhost --target)/lib
    -DLOCALSTATEDIR:PATH=/var
    -DOPENVAS_OMP_ONLY:BOOL=FALSE
    -DOPENVAS_PID_DIR:PATH=/run
    -DSYSCONFDIR:PATH=/etc
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'ldap LIBLDAP'
    'snmp SNMP'
)

src_install() {
    cmake_src_install

    keepdir /var/{cache,lib,log}/openvas

    # remove empty directories
    edo rmdir "${IMAGE}"/etc{/openvas{/gnupg,},}
    edo rmdir "${IMAGE}"/var/lib/openvas/gnupg
    edo rmdir "${IMAGE}"/run
    edo rmdir "${IMAGE}"/usr/share/openvas
}

