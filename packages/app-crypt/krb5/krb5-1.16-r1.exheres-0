# Copyright 2013 Dirk Heinrichs
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="MIT implementation of the Kerberos V protocol"
DESCRIPTION="
This is the original implementation of the Kerberos V protocol for secure authentication.
"
HOMEPAGE="https://web.mit.edu/kerberos"
DOWNLOADS="${HOMEPAGE}/dist/${PN}/$(ever range 1-2)/${PNV}.tar.gz"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~x86"
MYOPTIONS="
    ldap
    libedit
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# The tests want to run python, phone home and do all kinds of "funny" things.
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/gettext
    build+run:
        dev-libs/libverto
        sys-apps/keyutils
        sys-fs/e2fsprogs
        ldap? (
            net-directory/openldap
            net-libs/cyrus-sasl
        )
        libedit? ( dev-libs/libedit )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl[>=1.0.0] )
        !app-crypt/heimdal [[
            description = [ (MIT) krb5 and Heimdal collide. Choose one. ]
            url = [ http://dev.exherbo.org/~philantrop/heimdal-krb5-collisions.txt ]
            resolution = manual
        ]]
    suggestion:
        sys-auth/pam-krb5
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/libressl.patch
    -p2 "${FILES}"/e1caf6fb74981da62039846931ebdffed71309d1.patch
)

# --with-system-db is declared unsupported and untestet according to MIT docs.
DEFAULT_SRC_CONFIGURE_PARAMS=(
    # required as long as upstream uses autoconf < 2.70
    runstatedir=/run
    --enable-dns-for-realm
    --enable-nls
    --disable-rpath
    --with-crypto-impl=openssl
    --with-readline
    --with-system-et
    --with-system-ss
    --with-system-verto
    --with-tls-impl=openssl

    # NOTE: Assumes support for __attribute__((constructor)) in
    # order to fix cross-compiling. GCC and Clang support this.
    krb5_cv_attr_constructor_destructor=yes,yes
    # Both musl and glibc support regcomp and positional printf
    # specifiers, but krb5 can't check them when cross compiling
    ac_cv_func_regcomp=yes
    ac_cv_printf_positional=yes
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    ldap
    libedit
)

WORK=${WORKBASE}/${PNV}/src

src_install() {
    default

    # Remove empty directories
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/lib/krb5/plugins/{authdata,libkrb5}
    edo rmdir "${IMAGE}"/usr/share/man/cat?
    edo rm -r "${IMAGE}"/run

    keepdir /var/lib/krb5kdc

    # Move the examples to DOCDIR
    dodir /usr/share/doc/${PNVR}
    edo cp -r "${IMAGE}"/usr/share/examples/krb5 "${IMAGE}"/usr/share/doc/${PNVR}
    edo rm -r "${IMAGE}"/usr/share/examples

    install_systemd_files

    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins ${PN}-krb5kdc.conf <<EOF
d /run/krb5kdc 0755 root root
EOF

    hereconfd krb5kdc.conf <<EOF
# See krb5kdc(8) man page
KDC_ARGS=""
EOF
}

