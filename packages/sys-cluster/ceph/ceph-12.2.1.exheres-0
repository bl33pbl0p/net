# Copyright 2013 Kevin Decherf <kevin@kdecherf.com>
# Distributed under the terms of the GNU General Public License v2

require cmake [ api=2 cmake_minimum_version=2.8.11 ]

PLATFORMS="~amd64"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/hostname-fix.patch
)

SUMMARY="Ceph is a distributed object store and file system"
HOMEPAGE="https://ceph.com"
DOWNLOADS="https://download.ceph.com/tarballs/${PNV}.tar.gz"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    client   [[ description = [ Tools and libraries necessary for connecting to a Ceph cluster ] ]]
    debug    [[ description = [ Additional tools and tests for debugging Ceph clusters ] ]]
    gateway  [[ description = [ The RADOS Gateway, which provides OpenStack Swift and Amazon S3-compatible API access ] ]]
    fuse     [[ description = [ FUSE client for CephFS ] ]]
    man-pages
    profiler [[ description = [ Performance profiling with Google perf tools ] ]]
    client? ( ( providers: eudev systemd ) [[ number-selected = exactly-one ]] )
    "

DEPENDENCIES="
    build:
        dev-lang/yasm
        dev-lang/python:*
        dev-python/Cython
        dev-python/setuptools
        dev-python/virtualenv
        man-pages? ( dev-python/Sphinx )
    build+run:
        app-arch/lz4
        app-arch/snappy
        dev-db/leveldb
        dev-libs/boost
        dev-libs/jemalloc
        dev-libs/libaio
        dev-libs/libatomic_ops
        dev-libs/nss
        dev-python/PrettyTable
        sys-apps/keyutils
        sys-fs/xfsprogs
        client? (
            dev-libs/expat
            sys-apps/util-linux [[ note = [ libblkid ] ]]
            providers:eudev? ( sys-apps/eudev )
            providers:systemd? ( sys-apps/systemd )
        ) [[ note = [ rbd {map,unmapped,showmap} support ] ]]
        fuse? ( sys-fs/fuse )
        gateway? (
            dev-libs/expat
            net-misc/curl
        ) [[ note = [ uses bundled libs3 ] ]]
        profiler? ( dev-util/gperftools )
"

RESTRICT="test"

CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    'providers:systemd SYSTEMD'
    # The RADOS Block Device API - needed by qemu[ceph], among others
    'client RBD'
    # The RADOS Block Device API - needed by qemu[ceph], among others
    'client KRBD'
    # CephFS - the POSIX-compatible Ceph cluster filesystem
    'client CEPHFS'
    'fuse FUSE'
    'gateway RADOSGW'
    'man-pages MANPAGE'
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_INSTALL_PREFIX:PATH=/usr/$(exhost --target)
    -DCMAKE_INSTALL_FULL_SBINDIR:PATH=/usr/$(exhost --target)/bin
    -DCMAKE_INSTALL_SBINDIR=bin
    -DCMAKE_AR:PATH=/usr/host/bin/$(exhost --tool-prefix)ar
    # Disable unused stuff. Add matching option if you want to enable it.
    -DWITH_OPENLDAP=OFF
    -DWITH_RDMA=OFF
    -DWITH_LTTNG=OFF
    -DWITH_RADOSGW_FCGI_FRONTEND=OFF
    -DWITH_SELINUX=OFF
    -DWITH_BABELTRACE=OFF
    -DHAVE_BABELTRACE=OFF
    # Enable shared system boost. Add option if you want to disable
    -DWITH_SYSTEM_BOOST=ON
    -DWITH_NSS=ON
    -DWITH_XFS=ON
    # There should be better performances with jemalloc
    -DALLOCATOR="jemalloc"
)

src_prepare() {
    edo sed -i 's/DESTINATION sbin/DESTINATION bin/' ${WORKBASE}/${PNV}/src/CMakeLists.txt
    edo sed -i 's/CMAKE_INSTALL_LIBEXECDIR/CMAKE_INSTALL_LIBDIR/' ${WORKBASE}/${PNV}/systemd/CMakeLists.txt
    edo sed -i 's;/usr/lib/ceph/ceph-osd-prestart.sh;/usr/host/libexec/ceph/ceph-osd-prestart.sh;' \
        ${WORKBASE}/${PNV}/systemd/ceph-osd@.service
    cmake_src_prepare
}

src_configure() {
    LDFLAGS="${LDFLAGS} -lpthread" cmake_src_configure
}

src_install() {
    default

    keepdir /etc/${PN} /var/log/${PN} /var/lib/${PN}/tmp
}

