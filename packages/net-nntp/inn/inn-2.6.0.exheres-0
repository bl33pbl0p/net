# Copyright 2010-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'inn-2.5.0.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

SUMMARY="InterNetNews (INN) is a Usenet news server with integrated NNTP functionality"
DESCRIPTION="
INN (InterNetNews) is an extremely flexible and configurable Usenet / netnews news
server. It supports accepting articles via either NNTP connections or via UUCP,
as well as serving out articles to reading clients via NNTP. It supports multiple
different ways of sending outgoing articles to other hosts, as well as multiple
different storage mechanisms for articles and article overview information via
internal APIs. Compared to other news servers, INN is more complicated with a
steeper learning curve, but is extremely flexible and configurable, comes with a
large suite of supporting programs, and supports embedded filters in either Perl
or Python.
"
HOMEPAGE="http://www.eyrie.org/~eagle/software/${PN}"
DOWNLOADS="ftp://ftp.isc.org/isc/${PN}/${PNV}.tar.gz"

#BUGS_TO="philantrop@exherbo.org"
REMOTE_IDS="freecode:${PN}"

SLOT="0"
LICENCES="as-is BSD-2 BSD-3 GPL-2 MIT"
PLATFORMS="~amd64"
MYOPTIONS="berkdb kerberos perl python sasl ssl"

# Tries to bind to 0.0.0.0, access /dev/log and other funny stuff
RESTRICT="test"

DEPENDENCIES="
    build+run:
        berkdb? ( sys-libs/db:= )
        kerberos? ( app-crypt/heimdal )
        perl? (
            dev-lang/perl:=
            dev-perl/GD[>=2.44]
            dev-perl/MIME-tools[>=5.427]
        )
        python? ( dev-lang/python:= )
        sasl? ( net-libs/cyrus-sasl )
        ssl? ( virtual/libssl )
        virtual/mta
        group/news
        user/news
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
        --prefix=/usr/$(exhost --target)
        --bindir=/usr/$(exhost --target)/bin/news
        --infodir=/usr/share/info
        --mandir=/usr/share/man
        --sysconfdir=/etc/news
        --enable-ipv6
        --enable-keywords
        --enable-largefiles
        --enable-libtool
        --enable-setgid-inews
        --enable-uucp-rnews
        --with-control-dir=/usr/$(exhost --target)/bin/news/control
        --with-db-dir=/var/spool/news/db
        --with-doc-dir=/usr/share/doc/${PNVR}
        --with-filter-dir=/usr/$(exhost --target)/bin/news/filter
        --with-http-dir=/var/lib/news/http
        --with-news-user=news
        --with-news-group=news
        --with-news-master=usenet
        --with-log-dir=/var/log/news
        --with-run-dir=/run/news
        --with-spool-dir=/var/spool/news
        --with-syslog-facility=LOG_NEWS
        --with-tmp-dir=/var/spool/news/tmp
        --with-zlib
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "berkdb berkeleydb"
    kerberos
    perl
    python
    sasl
    "ssl openssl"
)

DEFAULT_SRC_INSTALL_EXCLUDE=( history )
DEFAULT_SRC_INSTALL_PARAMS=( SPECIAL="" )

src_install() {
    default

    #    edo chown -R root:0 "${IMAGE}"/usr/{$(exhost --target)/lib/news/{lib,include},share/{doc,man}}
    edo chmod 644 "${IMAGE}"/etc/news/*
    for file in control.ctl expire.ctl incoming.conf nntpsend.ctl passwd.nntp readers.conf
    do
        edo chmod 640 "${IMAGE}"/etc/news/${file}
    done

    dodoc doc/checklist
    edo rm "${IMAGE}"/usr/share/doc/${PNVR}/GPL

    # Prevent old db/* files from being overwritten
    insinto /usr/share/inn/dbexamples
    newins site/active.minimal active
    newins site/newsgroups.minimal newsgroups

    keepdir \
        /var/log/news \
        /var/spool/news/{,archive,articles,db,incoming{,/bad},innfeed,outgoing,overview,tmp}

    edo rmdir \
        "${IMAGE}"/run/{news,} \
        "${IMAGE}"/var/log/news/OLD

    insinto /usr/$(exhost --target)/include/news
    doins include/*.h

    for db_file in active newsgroups
    do
        [[ -f ${ROOT}/var/spool/news/db/${db_file} ]] && continue

        edo cp "${IMAGE}"/usr/share/inn/dbexamples/${db_file} "${IMAGE}"/var/spool/news/db/${db_file}
        edo chown news:news "${IMAGE}"/var/spool/news/db/${db_file}
        edo chmod 664 "${IMAGE}"/var/spool/news/db/${db_file}
    done
}

