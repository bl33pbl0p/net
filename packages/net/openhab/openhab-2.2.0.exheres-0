# Copyright 2017 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="openHAB - a vendor and technology agnostic open source automation software for your home"
DESCRIPTION="
openHAB is a software for integrating different home automation systems and technologies into one
single solution that allows over-arching automation rules and that offers uniform user interfaces.
"
HOMEPAGE="https://www.openhab.org"
DOWNLOADS="https://dl.bintray.com/${PN}/mvn/org/${PN}/distro/${PN}/${PV}/${PNV}.tar.gz"

UPSTREAM_DOCUMENTATION="https://docs.openhab.org [[ lang = en ]]"
UPSTREAM_RELEASE_NOTES="https://github.com/openhab/openhab-distro/releases/tag/${PV} [[ lang = en ]]"

LICENCES="EPL-1.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        group/audio
        group/dialout
        group/tty
        group/${PN}
        user/${PN}
    run:
        virtual/jre:*[>=1.8]
"

WORK=${WORKBASE}

src_test() {
    :
}

src_install() {
    insinto /etc/default
    doins "${FILES}"/${PN}$(ever major)
    insinto /etc/profile.d/
    doins "${FILES}"/${PN}$(ever major).sh
    insinto /etc/${PN}$(ever major)
    doins -r conf/*

    insinto /usr/share/${PN}$(ever major)
    doins -r addons runtime
    exeinto /usr/share/${PN}$(ever major)
    doexe *.sh

    insinto /var/lib/${PN}$(ever major)
    doins -r userdata/*

    keepdir /var/lib/${PN}$(ever major)/persistence/{db4o,mapdb,rrd4j}
    keepdir /var/log/${PN}$(ever major)

    edo rm "${IMAGE}"/usr/share/${PN}$(ever major)/runtime/bin/*.{bat,ps1}
    edo rm -rf "${IMAGE}"/usr/share/${PN}$(ever major)/runtime/bin/contrib

    edo chmod 0755 "${IMAGE}"/usr/share/${PN}$(ever major)/runtime/bin/{backup,client,inc,instance,karaf,oh2_dir_layout,restore,setenv,shell,start,status,stop,update}
    edo chown -R openhab:openhab "${IMAGE}"/etc/${PN}$(ever major)
    edo chown -R openhab:openhab "${IMAGE}"/usr/share/${PN}$(ever major)
    edo chown -R openhab:openhab "${IMAGE}"/var/{lib,log}/${PN}$(ever major)

    install_systemd_files
}

